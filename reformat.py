'''
@author: derrick
'''
import csv
import sys
import urllib2
import json
import urllib

api_key = "bahzgqqdvtchukvz3zbjnhe3"

class CSVProcessor:
	def __init__(self, csvName):
		self.countsArray = [{}, {}, {}] # array of jsons, which map month-year => count
		csvFile = open(csvName, 'rb')
		self.reader = csv.reader(csvFile, delimiter=',')
		

	def processAndWrite(self, outFileName):
		of = open(outFileName ,'w')
		ct = 0
		for row in self.reader:
			ct = ct + 1
			if ct == 1:
				row.append("description")
				self.writeHeader(row ,of)
				continue
			if self.isMobile(row):
				companyName = self.getCompanyName(row);
				if len(companyName) > 0:
					print ct
					print companyName
					description = self.getDescription(companyName)
					print description
					row.append(description)
					try:
						of.write(",".join(row)+"\n")
					except:
						continue
		of.close()

	def writeHeader(self, row , of):
		of.write(",".join(row))

	def isMobile(self, rowArr):
		'''
		category - 2
		'''
		return rowArr[2].strip() == "mobile"

	def getCompanyName(self, rowArr):
		return rowArr[1].strip()

	def getDescription(self, companyName):
		url = "http://api.crunchbase.com/v/1/company/" + urllib.quote_plus(companyName) + ".js?api_key=" + api_key
		try:
			response = urllib2.urlopen(url).read()
			lr = json.loads(response)
			description = lr["description"]
		except:
			description = ""
		return description

	# def writeCounts(self, writeFileName):
	# 	f = open(writeFileName ,'w')
	# 	"""
	# 	for counts in self.countsArray:
	# 		counts = sorted(counts, key=lambda date: date.split("/")[1])
	# 	"""
	# 	for d in self.countsArray[0]:
	# 		f.write(d + "," + ",".join([str(count[d]) for count in self.countsArray]) + "\n")
	# 	f.close()
	# 	return


	
if __name__ == "__main__":
	inFileName = sys.argv[1]
	outFileName = sys.argv[2]
	csvWriter = CSVProcessor(inFileName)
	csvWriter.processAndWrite(outFileName)

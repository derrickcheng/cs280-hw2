%addpath('liblinear-1.92/matlab')
load('sparseTFIDFMatrix.csv');
S = spconvert(sparseTFIDFMatrix);
D = full(S);
L = load('labels.csv')
% obj = ClassificationDiscriminant.fit(S,labels)
% model = train(train_labels_vec, sparse(train_instance_mat), '-s 2','row')
% model = train(L, S, '-s l2 -p 0')

% http://www.mathworks.com/help/stats/nlinfit.html
% http://www.mathworks.com/help/stats/fitlm.html
% http://www.mathworks.com/help/stats/regress.html
BETA = regress(L, D)
csvwrite('beta.csv',BETA)

% [predicted_label, accuracy, prob_estimates] = predict(L, S, model)
%errorRate = 100.0 - accuracy(1)
%errorRate
%rmpath('liblinear-1.92/matlab')
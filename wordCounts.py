'''
@author: derrick
'''
import csv
import sys
import re 
import operator

class CSVProcessor:
	def __init__(self, csvName):
		self.countsArray = [{}, {}, {}] # array of jsons, which map month-year => count
		csvFile = open(csvName, 'rb')
		self.reader = csv.reader(csvFile, delimiter=',')
		self.wordCounts = {}
		self.stopWords = []

	# counts up description
	def process(self, outFileName):
		of = open(outFileName ,'w')
		ct = 0
		self.loadStopWords()
		for row in self.reader:
			ct = ct + 1
			if ct == 1:
				continue
			# update count
			self.updateCounts(row)
		self.writeCounts(of)
		of.close()
	
	def updateCounts(self, row):
		description = row[len(row) -1]
		for word in description.split(r" "):
			print word
			m = re.sub(r"[^a-zA-Z0-9]", r"", word)
			m = m.strip()
			if m:
				cleaned = m.lower()
				if cleaned and cleaned not in self.stopWords:
					print cleaned
					if cleaned in self.wordCounts:
						self.wordCounts[cleaned] += 1
					else:
						self.wordCounts[cleaned] = 1

	def writeCounts(self, of):
		x = self.wordCounts
		sorted_x = sorted(x.iteritems(), key=operator.itemgetter(1))
		sorted_x.reverse()
		print sorted_x
		ct = 1
		for key,val in sorted_x:
			of.write(str(ct) + "," + key + "," + str(val) + "\n")
			ct += 1

	def loadStopWords(self):
		stopWordsFile = open("stopWords.csv", 'rb')
		stopReader = csv.reader(stopWordsFile, delimiter=',')
		for row in stopReader:
			self.stopWords = self.stopWords + row

if __name__ == "__main__":
	inFileName = sys.argv[1]
	outFileName = sys.argv[2]
	csvWriter = CSVProcessor(inFileName)
	csvWriter.process(outFileName)

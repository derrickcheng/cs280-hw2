#!/bin/bash

# create dictionary
python wordCounts.py fullAndDescription.csv wordCounts.csv

# create featurization for non tfidf
python featurize.py fullAndDescription.csv wordCounts.csv sparseMatrix.csv labels.csv f

# create featurization for tfidf
python featurize.py fullAndDescription.csv wordCounts.csv sparseTFIDFMatrix.csv labels.csv t

# create joinedBeta not for TFIDF
python beta.py wordCounts.csv beta.csv joinedNonBeta.csv

# create joinedBeta for TFIDF
python beta.py wordCounts.csv beta.csv joinedTFIDFBeta.csv

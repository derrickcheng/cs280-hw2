'''
@author: derrick
'''
import csv
import sys
import re 
import operator
from math import log

class CSVProcessor:
	def __init__(self, csvName, dictFileName, isTFIDF):
		csvFile = open(csvName, 'rb')
		self.reader = csv.reader(csvFile, delimiter=',')
		self.dictFileName = dictFileName
		self.dict = {}
		self.isTFIDF = isTFIDF
		self.tf = {} #overall term freq

	def process(self, outFileName, labelsOutFileName):
		of = open(outFileName ,'w')
		lf = open(labelsOutFileName, 'w')
		rowCt = 1
		first = True
		self.loadDict(self.dictFileName)
		for row in self.reader:
			if first:
				first = False
				continue
			# only write if funding exists and description exists
			description = row[len(row) -1]
			funding = row[3]
			if description and funding:
				print rowCt
				print description
				self.writeSparseRow(row, of, rowCt)
				# row[1] + "," + 
				lf.write(row[3] + "\n") # only write if successful
				rowCt += 1
		of.close()
		lf.close()

	def loadDict(self, dictFileName):
		dictFile = open(dictFileName, 'rb')
		dictReader = csv.reader(dictFile, delimiter=',')
		for row in dictReader:
			self.dict[row[1]] = row[0]
			self.tf[row[1]] =  int(row[2])
		#print self.dict

	def writeSparseRow(self, row, of, rowCt):
		description = row[len(row) -1]
		wordCounts = {}
		for word in description.split(" "):
			cleaned = re.sub(r"[^a-zA-Z0-9]", r"", word)
			cleaned = cleaned.strip()
			cleaned = cleaned.lower()
			if cleaned and (cleaned in self.dict):
				self.updateWordCounts(cleaned.lower(), wordCounts)
		print rowCt
		for k,v in wordCounts.iteritems():
			if self.isTFIDF == 't':
				v = log(657/ self.tf[k]) * v
			of.write(str(rowCt) + "," + self.dict[k] + "," + str(v) + "\n")
		return

	def updateWordCounts(self, cleaned, wordCounts):
		if cleaned not in wordCounts:
			wordCounts[cleaned] = 1
		else:
			wordCounts[cleaned] += 1

if __name__ == "__main__":
	inFileName = sys.argv[1] # fullAndDescription.csv
	dictFileName = sys.argv[2] # wordCounts.csv
	outFileName = sys.argv[3] # sparseMatrix.csv
	labelsOutFileName = sys.argv[4] # labels.csv
	isTFIDF = sys.argv[5] # t/f
	csvWriter = CSVProcessor(inFileName, dictFileName, isTFIDF)
	csvWriter.process(outFileName, labelsOutFileName)

'''
@author: derrick
'''
import csv
import sys
import re 
import operator

class Joiner:
	def __init__(self, outFileName):
		self.outFileName = outFileName
		return

	def go(self, dictFileName, betaFileName):
		of = open(self.outFileName, 'w')
		d = self.getDict(dictFileName)
		b = self.getBeta(betaFileName)
		j = {}
		for index, coef in b.iteritems():
			j[d[index]] = coef
		sorted_j = sorted(j.iteritems(), key=operator.itemgetter(1))
		sorted_j.reverse()
		print sorted_j
		for key,val in sorted_j:
			of.write(key + "," + str(val) + "\n")
		of.close()
		return

	def getBeta(self, f):
		file = open(f, 'rb')
		d = {}
		fileReader = csv.reader(file, delimiter=',')
		ct = 1
		for row in fileReader:
			d[ct] = row[0]
			ct += 1
		return d

	def getDict(self, f):
		fo = open(f, 'rb')
		d = {}
		fileReader = csv.reader(fo, delimiter=',')
		for row in fileReader:
			d[int(row[0])] = row[1]
		return d
			

if __name__ == "__main__":
	dictFileName = sys.argv[1] # wordCounts.csv
	betaFileName = sys.argv[2] # beta.csv
	outFileName = sys.argv[3] # joinedBeta.csv
	joiner = Joiner(outFileName)
	joiner.go(dictFileName, betaFileName)
	
